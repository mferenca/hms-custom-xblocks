/* Javascript for InteractiveHTML5XBlock. */
function InteractiveHTML5XBlock(runtime, element) {

  $(function ($) {

    $('#button-wrapp a').on('click', function(e) {
      $('#wrapper-modal').modal({
        clickClose: false
      });
      $('.sequence-nav, .sequence-bottom').addClass('reduceZIndex');
    });
        

    // center the big row of lymphocytes and the initial cell
    $('#row5').on("load", centerWideRow);
    $('#row1, #row1glow, #row2, #row3, #row4').on("load", centerElement);

    $(window).on("resize", function() {
      $('#row1, #row1glow, #row2, #row3, #row4').each(centerElement);
      $('#row5').each(centerWideRow);
    });

    // first cell is dynamically positioned so set the microbe target values
    // after this is positioned
    var microbeTarget;
    $('#row1').on("load", function() {
      microbeTarget = $(this).offset();
    });

    var targetPos = $('#row1').position();
    microbeTarget = targetPos;

    // hide the cell glow and the expansion cells
    var $lGlowing = $('#row1glow');
    $lGlowing.hide();
    $('#expansion-container img').hide();
     
    // handle dragging the microbe
    var microbeLymphocyteOffset = { leftDelta: 147, topDelta: -23 };
    var tolerance = 5;
    $microbe = $('#microbe');
    $microbe.draggable({ containment: "parent" });
    
    $microbe.on('dragstart', function() {
      microbeTarget = $('#row1').offset();
    });

    $microbe.on("drag", function(event, microbePosition) {
      // if bug position is within range of correct microbeLymphocyteOffset then snap
        
      var closeEnough = close(microbePosition, microbeTarget);
      var glowing = ($lGlowing.css("display") != "none");

      if (closeEnough && !glowing) {
        $lGlowing.show();
      }
      else if (!closeEnough && glowing) {
        $lGlowing.hide();
      }
    });

    $microbe.on("dragstop", function(event, microbePosition) {
      if (close(microbePosition, microbeTarget)) {
        var goalLeft = microbeLymphocyteOffset.leftDelta + microbeTarget.left;
        var goalTop = Math.abs(microbeLymphocyteOffset.topDelta);

        $microbe.animate({left: goalLeft + "px", top: goalTop + "px"}, 'fast',
          function() {
            $('#expansion-container img').each(function(i) {
              $(this).delay(i * 500).fadeIn('fast');
            });
          }
        );
        $('#recognitionSound')[0].play();
      }
    });

    function windowCenterX() {
      return $(window).width()/2.0;
    };

    function centerElement() {
      var myWidth = $(this).width();
      $(this).css({"margin-left": windowCenterX()-myWidth/2.0 + "px"});
    }

    function centerWideRow() {
      var myWidth = $(this).width();
      $(this).css({ "margin-left": windowCenterX()-myWidth/2.0-20.0 + "px"});    
    }

    function close(microbePosition, microbeTarget) {
      function within(actual, expected, tolerance) {
        return Math.abs(actual - expected) <= tolerance;
         
      }
      var withinXRange = within(microbePosition.offset.left - microbeTarget.left,
        microbeLymphocyteOffset.leftDelta, tolerance);
      var withinYRange = within(microbePosition.offset.top - microbeTarget.top, 
        Math.abs(microbeLymphocyteOffset.topDelta), tolerance);
      return withinXRange && withinYRange;
    }

  });
}
