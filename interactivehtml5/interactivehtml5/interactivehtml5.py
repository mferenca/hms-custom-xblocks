"""TO-DO: Write a description of what this XBlock is."""

import pkg_resources

from xblock.core import XBlock
from xblock.fields import Scope, Integer
from xblock.fragment import Fragment


class InteractiveHTML5XBlock(XBlock):
    """
    TO-DO: document what your XBlock does.
    """

    # Fields are defined on the class.  You can access them in your code as
    # self.<fieldname>.

    # TO-DO: delete count, and define your own fields.
    def resource_string(self, path):
        """Handy helper for getting resources from our kit."""
        data = pkg_resources.resource_string(__name__, path)
        return data.decode("utf8")

    # TO-DO: change this view to display your data your own way.
    def student_view(self, context=None):
        """
        The primary view of the InteractiveHTML5XBlock, shown to students
        when viewing courses.
        """
        html = self.resource_string("static/html/interactivehtml5.html")
        frag = Fragment(html.format(self=self))
        frag.add_css(self.resource_string("static/css/interactivehtml5.css"))
        frag.add_css(self.resource_string("static/css/jquery.modal.css"))
        frag.add_css_url("http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css")
        frag.add_javascript(self.resource_string("static/js/src/jquery.modal.js"))
        frag.add_javascript(self.resource_string("static/js/src/interactivehtml5.js"))
        frag.add_javascript_url("http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js")
        frag.initialize_js('InteractiveHTML5XBlock')
        return frag

    def studio_view(self, context=None):
        html = self.resource_string("static/html/interactivehtml5studio.html")
        frag = Fragment(html.format(self=self))
        frag.add_css(self.resource_string("static/css/interactivehtml5.css"))
        return frag

    # TO-DO: change this handler to perform your own actions.  You may need more
    # than one handler, or you may not need any handlers at all.
    # @XBlock.json_handler


    # TO-DO: change this to create the scenarios you'd like to see in the
    # workbench while developing your XBlock.
    @staticmethod
    def workbench_scenarios():
        """A canned scenario for display in the workbench."""
        return [
            ("InteractiveHTML5XBlock",
             """<vertical_demo>
                <interactivehtml5/>
                </vertical_demo>
             """),
        ]
