"""TO-DO: Write a description of what this XBlock is."""

import pkg_resources

from xblock.core import XBlock
from xblock.fields import Scope, Integer
from xblock.fragment import Fragment


class DiveXBlock(XBlock):
    """
    TO-DO: document what your XBlock does.
    """

    # Fields are defined on the class.  You can access them in your code as
    # self.<fieldname>.

    # TO-DO: delete count, and define your own fields.

    def get_icon_class(self):
        """
        Return a css class identifying this module in the context of an icon
        """
        return "nesto"

    def resource_string(self, path):
        """Handy helper for getting resources from our kit."""
        data = pkg_resources.resource_string(__name__, path)
        return data.decode("utf8")

    # TO-DO: change this view to display your data your own way.
    def student_view(self, context=None):
        """
        The primary view of the DiveXBlock, shown to students
        when viewing courses.
        """
        html = self.resource_string("static/html/dive.html")
        frag = Fragment(html.format(self=self))
        frag.add_css(self.resource_string("static/css/dive.css"))
        frag.add_javascript(self.resource_string("static/js/src/dive.js"))
        frag.initialize_js('DiveXBlock')
        return frag

    # TO-DO: change this handler to perform your own actions.  You may need more
    # than one handler, or you may not need any handlers at all.

    # TO-DO: change this to create the scenarios you'd like to see in the
    # workbench while developing your XBlock.
    @staticmethod
    def workbench_scenarios():
        """A canned scenario for display in the workbench."""
        return [
            ("DiveXBlock",
             """<vertical_demo>
                <dive/>
                <dive/>
                <dive/>
                </vertical_demo>
             """),
        ]
