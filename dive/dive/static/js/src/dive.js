/* Javascript for DiveXBlock. */
function DiveXBlock(runtime, element) {

    $(function ($) {
        /* Here's where you'd do things on page load. */
        $('.dive-button-wrapper').click(function() {

        	$(".dive-iframe").addClass("dive-iframe-fullscreen");
        	$("body").append("<div class='dive-button-wrapper dive-button-fullscreen'><a>Exit</a></div>");
            $("body").css("overflow", "hidden");
            window.scrollTo(0, 0);
    	});

        $(document).delegate('.dive-button-fullscreen', 'click', function() {
            console.log("clikc");
            $(".dive-iframe").removeClass("dive-iframe-fullscreen");
            $("body").children("div.dive-button-wrapper").remove();
            $("body").css("overflow", "auto");
        });
    });
}