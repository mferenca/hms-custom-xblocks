"""TO-DO: Write a description of what this XBlock is."""

import pkg_resources
from django.template import Context, Template

from xblock.core import XBlock
from xblock.fields import Scope, Integer, String
from xblock.fragment import Fragment


class InteractiveXBlock(XBlock):
    """
    TO-DO: document what your XBlock does.
    """

    # Fields are defined on the class.  You can access them in your code as
    # self.<fieldname>.

    # TO-DO: delete count, and define your own fields.

    display_name = String(
        display_name = "Display Name",
        default="Interactive",
        scope=Scope.settings,
    )

    src = String(
        default="", scope=Scope.content,
        help="URL that you want to display in Iframe.",
    )

    imageSrc = String(
        default="", scope=Scope.content,
        help="URL of the image to use as expand button.",
    )

    exitSrc = String(
        default="", scope=Scope.content,
        help="URL of the image to use as exit button.",
    )

    buttonTop = Integer(
        default="160", scope=Scope.content,
        help="Exit button's top coordinate",
    )

    buttonLeft = Integer(
        default="44", scope=Scope.content,
        help="Exit butoon's left coordinate",
    )

    buttonColor = String(
        default="#000", scope=Scope.content,
        help="The color of the exit button",
    )

    buttonPosition = String(
        default="Top-Right", scope=Scope.content,
        help="The color of the exit button",
    )

    def load_resource(self, resource_path):
        """
        Gets the content of a resource
        """
        resource_content = pkg_resources.resource_string(__name__, resource_path)
        return unicode(resource_content)

    def render_template(self, template_path, context={}):
        """
        Evaluate a template by resource path, applying the provided context
        """
        template_str = self.load_resource(template_path)
        return Template(template_str).render(Context(context))

    # TO-DO: change this view to display your data your own way.
    def student_view(self, context=None):
        """
        The primary view of the InteractiveXBlock, shown to students
        when viewing courses.
        """

        context = {
            'display_name': self.display_name,
            'src': self.src,
            'buttonTop': self.buttonTop,
            'buttonLeft': self.buttonLeft,
            'buttonColor': self.buttonColor,
            'imageSrc': self.imageSrc,
            'exitSrc': self.exitSrc,
            'buttonPosition': self.buttonPosition,
        }

        html = self.render_template("static/html/interactive.html", context)
        frag = Fragment(html)
        frag.add_css(self.load_resource("static/css/interactive.css"))
        frag.add_javascript(self.load_resource("static/js/src/interactive.js"))
        frag.initialize_js('InteractiveXBlock')
        return frag

    # TO-DO: change this view to display your data your own way.
    def studio_view(self, context=None):
        """
        The primary view of the Audio2XBlock, shown to students
        when viewing courses.
        """

        context = {
            'display_name': self.display_name,
            'src': self.src,
            'buttonTop': self.buttonTop,
            'buttonLeft': self.buttonLeft,
            'buttonColor': self.buttonColor,
            'imageSrc': self.imageSrc,
            'exitSrc': self.exitSrc,
            'buttonPosition': self.buttonPosition,
        }

        html = self.render_template('static/html/interactive_edit.html', context)
        frag = Fragment(html)

        frag.add_javascript(self.load_resource("static/js/src/interactive_edit.js"))
        frag.initialize_js('InteractiveEditXBlock')
        return frag

    # TO-DO: change this handler to perform your own actions.  You may need more
    # than one handler, or you may not need any handlers at all.
    @XBlock.json_handler
    def save_interactive(self, data, suffix=''):
        """
        An example handler, which increments the data.
        """
        self.display_name = data['display_name']
        self.src = data['src']
        self.buttonTop = data['buttonTop']
        self.buttonLeft = data['buttonLeft']
        self.buttonColor = data['buttonColor']
        self.imageSrc = data['imageSrc']
        self.exitSrc = data['exitSrc']
        self.buttonPosition = data['buttonPosition']

        return {
            'result': 'success',
        }


    # TO-DO: change this to create the scenarios you'd like to see in the
    # workbench while developing your XBlock.
    @staticmethod
    def workbench_scenarios():
        """A canned scenario for display in the workbench."""
        return [
            ("InteractiveXBlock",
             """<vertical_demo>
                <interactive/>
                <interactive/>
                <interactive/>
                </vertical_demo>
             """),
        ]
