/* Javascript for InteractiveXBlock. */
function InteractiveXBlock(runtime, element) {

    $(function ($) {
        /* Here's where you'd do things on page load. */

        var windowWidth = $(window).width();
        var top = $(element).find(".buttonTop").text();
        var left = $(element).find(".buttonLeft").text();
        var color = $(element).find(".buttonColor").text();
        var imageSrc = $(element).find(".imageSrc").text();
        var exitSrc = $(element).find(".exitSrc").text();
        var buttonPosition = $(element).find(".buttonPosition").text();

        if(buttonPosition == "Custom"){
            $(element).find(".interactive-button-fullscreen").css({ 'top' : top+"px", 'left' : left+"px"});
        }
        else{
            $(element).find(".interactive-button-fullscreen").addClass(buttonPosition);
        }

        if(buttonPosition=="Top-Center"){

            $(".Top-Center").css({ 'left' : (windowWidth/2)-20+"px"});
        }
        else if(buttonPosition=="Top-Right"){

            $(".Top-Right").css({ 'left' : windowWidth-80+"px"});
        }

        if(imageSrc){
            $(element).find(".iframe-wrapper").css({ 'margin-left' : "9999px", 'margin-top': '-600px'});
            $(element).find('.interactive-button-wrapper').css({ 'height' : "600px", "background-size": "contain", "background-repeat": "no-repeat", "background-image": "url("+imageSrc+")"});
            $(element).find('.interactive-button-wrapper').children("p").text("");
        }
        else{
            $(element).find('.interactive-button-wrapper').children("p").text("Expand");
        }

        $(element).find('.interactive-button-wrapper').click(function() {

            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                window.open($(".interactive-iframe").attr("src"), '_blank');
            }
            else{

                $(element).find(".iframe-wrapper").css({ 'margin-left' : "0px", 'margin-top': '0px'});
                $(element).find(".iframe-wrapper").addClass("mobile-iframe");
                $(element).find(".interactive-iframe").addClass("interactive-iframe-fullscreen");
                
                $("body").append("<div class='interactive-button-fullscreen'><p>Exit</p></div>");

                if(buttonPosition == "Custom"){
                    $(".interactive-button-fullscreen").css({ 'top' : top+"px", 'left' : left+"px"});
                }
                else{
                    $(".interactive-button-fullscreen").addClass(buttonPosition);
                }

                if(buttonPosition=="Top-Center"){
                    $(".Top-Center").css({ 'left' : (windowWidth/2)-20+"px"});
                }
                else if(buttonPosition=="Top-Right"){
                    $(".Top-Right").css({ 'left' : windowWidth-80+"px"});
                }

                $("body").css("overflow", "hidden");
                window.scrollTo(0, 0);

                if(exitSrc){
                    $(".interactive-button-fullscreen p").css({ 'display' : "none"});
                    $(".interactive-button-fullscreen").css({ "background-image": "url("+exitSrc+")", "background-size": "cover", "width":"40px", "height": "40px", "border":"none"});
                }
                else{
                    $(".interactive-button-fullscreen").css({'color' : color, 'border-color' : color});
                    $(".interactive-button-fullscreen p").css({ 'color' : color});    
                }
            }

        });

        $(document).delegate('.interactive-button-fullscreen', 'click', function() {

            imageSrc = $(element).find(".imageSrc").text();

            if(imageSrc){
                $(element).find(".iframe-wrapper").css({ 'margin-left' : "9999px", 'margin-top': '-600px'});
            }
            $(element).find(".iframe-wrapper").removeClass("mobile-iframe");
            $(element).find(".interactive-iframe").removeClass("interactive-iframe-fullscreen");
            $("body").children("div.interactive-button-fullscreen").remove();
            $("body").css("overflow", "auto");
        });

        $(window).resize(function() {
            windowWidth = $(window).width();
            $(".Top-Center").css({ 'left' : (windowWidth/2)-20+"px"});
            $(".Top-Right").css({ 'left' : windowWidth-80+"px"});
        });
    });
}