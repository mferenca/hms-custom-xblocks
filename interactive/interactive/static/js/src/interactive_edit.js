/* Javascript for InteractiveXBlock. */
function InteractiveEditXBlock(runtime, element) {

    $(function ($) {
        if ($("#interactive_exit_src").val().length > 0) {
            $("#interactive_edit_buttoncolor").attr('disabled', "true");
        } else {
            $("#interactive_edit_buttoncolor").removeAttr('disabled');
        }

        var selected = $( "#exit_button_position option:selected" ).text();
        
        if (selected != "Custom") {
            $('#interactive_edit_buttontop').attr('disabled', "true");
            $('#interactive_edit_buttonleft').attr('disabled', "true");
        } else {
            $('#interactive_edit_buttontop').removeAttr('disabled');
            $('#interactive_edit_buttonleft').removeAttr('disabled');
        }
    });

    /* Html element for displaying error messages */
    $('.xblock-editor-error-message', element).html();
    $('.xblock-editor-error-message', element).css('display', 'none');
    $('.xblock-editor-error-message', element).css('color', 'red');

    $(element).find('.action-cancel').bind('click', function() {
            runtime.notify('cancel', {});
    });

    $(element).find('.action-save').bind('click', function() {
        var data = {
            'display_name': $('#interactive_edit_name').val(),
            'src': $('#interactive_edit_src').val(),
            'buttonTop': $('#interactive_edit_buttontop').val(),
            'buttonLeft': $('#interactive_edit_buttonleft').val(),
            'buttonColor': $('#interactive_edit_buttoncolor').val(),
            'imageSrc': $('#interactive_img_src').val(),
            'exitSrc': $('#interactive_exit_src').val(),
            'buttonPosition': $("#exit_button_position").find(":selected").val(),
        };
        
        runtime.notify('save', {state: 'start'});
        
        var handlerUrl = runtime.handlerUrl(element, 'save_interactive');
        $.post(handlerUrl, JSON.stringify(data)).done(function(response) {
            if (response.result === 'success') {
                runtime.notify('save', {state: 'end'});
                var windowWidth = $(window).width();
                $(".Top-Center").css({ 'left' : (windowWidth/2)-20+"px"});
                $(".Top-Right").css({ 'left' : windowWidth-80+"px"});
            } else {
                runtime.notify('error', {msg: response.message})
            }
        });
    });

    $(element).on('keyup', 'input#interactive_exit_src', function(){
        var top = $(this).val();
        console.log(top.length);
        if (top.length > 0) {
            $("#interactive_edit_buttoncolor").attr('disabled', "true");
        } else {
            $("#interactive_edit_buttoncolor").removeAttr('disabled');
        }
    });

    $(element).on('keyup', 'input#interactive_edit_buttontop', function(){
        var top = $(this).val();
        
        if (isNaN(top)) {
            $('.xblock-editor-error-message', element).html('Please enter a valid integer.');
            $('.xblock-editor-error-message', element).css('display', 'block');
            $(this).val("");
        } else {
            $('.xblock-editor-error-message', element).css('display', 'none');
        }
    });

    $(element).on('keyup', 'input#interactive_edit_buttonleft', function(){
        var left = $(this).val();
        
        if (isNaN(left)) {
            $('.xblock-editor-error-message', element).html('Please enter a valid integer.');
            $('.xblock-editor-error-message', element).css('display', 'block');
            $(this).val("");
        } else {
            $('.xblock-editor-error-message', element).css('display', 'none');
        }
    });

    $(element).on('change', 'select#exit_button_position', function(){
        var selected = $( "#exit_button_position option:selected" ).text();
        
        if (selected != "Custom") {
            $('#interactive_edit_buttontop').attr('disabled', "true");
            $('#interactive_edit_buttonleft').attr('disabled', "true");
        } else {
            $('#interactive_edit_buttontop').removeAttr('disabled');
            $('#interactive_edit_buttonleft').removeAttr('disabled');
        }
    });
}