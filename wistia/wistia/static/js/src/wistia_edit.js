/* Javascript for WistiaXBlock. */
function WistiaEditXBlock(runtime, element) {

    $(element).find('.action-cancel').bind('click', function() {
        runtime.notify('cancel', {});
    });

    $(element).find('.action-save').bind('click', function() {
        var data = {
           'url_src': $('#wistia_edit_src').val(),
        };
        
        runtime.notify('save', {state: 'start'});
        
        var handlerUrl = runtime.handlerUrl(element, 'save_wistia');
        $.post(handlerUrl, JSON.stringify(data)).done(function(response) {
            if (response.result === 'success') {
                runtime.notify('save', {state: 'end'});
            } else {
                runtime.notify('error', {msg: response.message})
            }
        });
    });

    $(function ($) {
        /* Here's where you'd do things on page load. */
    });
}
