"""TO-DO: Write a description of what this XBlock is."""

import pkg_resources
from django.template import Context, Template

from xblock.core import XBlock
from xblock.fields import Scope, String
from xblock.fragment import Fragment


class WistiaXBlock(XBlock):
    """
    TO-DO: document what your XBlock does.
    """

    # Fields are defined on the class.  You can access them in your code as
    # self.<fieldname>.

    # TO-DO: delete count, and define your own fields.
    url_src = String(
        default="", scope=Scope.content,
        help="URL of the Wistia video.",
    )

    def load_resource(self, resource_path):
        """
        Gets the content of a resource
        """
        resource_content = pkg_resources.resource_string(__name__, resource_path)
        return unicode(resource_content)

    def render_template(self, template_path, context={}):
        """
        Evaluate a template by resource path, applying the provided context
        """
        template_str = self.load_resource(template_path)
        return Template(template_str).render(Context(context))

    # TO-DO: change this view to display your data your own way.
    def student_view(self, context=None):
        """
        The primary view of the WistiaXBlock, shown to students
        when viewing courses.
        """

        context = {
            'url_src': self.url_src,
        }

        html = self.render_template("static/html/wistia.html", context)
        frag = Fragment(html)
        frag.add_css(self.load_resource("static/css/wistia.css"))
        frag.add_javascript(self.load_resource("static/js/src/wistia.js"))
        frag.initialize_js('WistiaXBlock')
        return frag

        # TO-DO: change this view to display your data your own way.
    def studio_view(self, context=None):
        """
        The primary view of the Audio2XBlock, shown to students
        when viewing courses.
        """

        context = {
            'url_src': self.url_src,
        }

        html = self.render_template('static/html/wistia_edit.html', context)
        frag = Fragment(html)

        frag.add_javascript(self.load_resource("static/js/src/wistia_edit.js"))
        frag.initialize_js('WistiaEditXBlock')
        return frag

    # TO-DO: change this handler to perform your own actions.  You may need more
    # than one handler, or you may not need any handlers at all.
    @XBlock.json_handler
    def save_wistia(self, data, suffix=''):
        """
        An example handler, which increments the data.
        """
        self.url_src = data['url_src']

        return {
            'result': 'success',
        }

    # TO-DO: change this to create the scenarios you'd like to see in the
    # workbench while developing your XBlock.
    @staticmethod
    def workbench_scenarios():
        """A canned scenario for display in the workbench."""
        return [
            ("WistiaXBlock",
             """<vertical_demo>
                <wistia/>
                <wistia/>
                <wistia/>
                </vertical_demo>
             """),
        ]
